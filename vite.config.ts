import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';
import * as path from 'path';

const config: UserConfig = {
	plugins: [sveltekit()],
	define: {
		global: "window",
	},
	resolve: {
		alias: { "@": path.resolve(__dirname, "src") }
	}
};

export default config;
