#!/bin/sh

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

cd "${ROOT_DIR}" || exit 1

if [ -f ".env" ] ;then
    VITE_PUBLIC_URL=$(grep VITE_PUBLIC_URL .env | cut -d '=' -f2-)
    if [ -z "$VITE_PUBLIC_URL" ] ;then
        echo "VITE_PUBLIC_URL not found in .env file"
        exit 0
    fi
    manifest_light="${ROOT_DIR}/public/site_light.webmanifest"
    manifest_dark="${ROOT_DIR}/public/site_dark.webmanifest"
    if [ -f "${manifest_light}" ]; then
        sed "s|\"scope\".*|\"scope\": ${VITE_PUBLIC_URL},|" "${manifest_light}" > "${manifest_light}.tmp" && mv "${manifest_light}.tmp" "${manifest_light}"
        sed "s|\"start_url\".*|\"start_url\": ${VITE_PUBLIC_URL},|" "${manifest_light}" > "${manifest_light}.tmp" && mv "${manifest_light}.tmp" "${manifest_light}"
    fi
    if [ -f "${manifest_dark}" ]; then
        sed "s|\"scope\".*|\"scope\": ${VITE_PUBLIC_URL},|" "${manifest_dark}" > "${manifest_dark}.tmp" && mv "${manifest_dark}.tmp" "${manifest_dark}"
        sed "s|\"start_url\".*|\"start_url\": ${VITE_PUBLIC_URL},|" "${manifest_dark}" > "${manifest_dark}.tmp" && mv "${manifest_dark}.tmp" "${manifest_dark}"
    fi
fi
