
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte'
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: vitePreprocess(),
	kit: {
		appDir: 'static',
		adapter: adapter({
			pages: 'public',
			assets: 'public',
			fallback: 'index.html',
			precompress: true,
			strict: true
		}),
		alias: {
			'@': './src'
		},
		paths: {
			base: '/collect'
		},
		prerender: { entries: [] },
		csp: {
			directives: {
				'script-src': ['self']
			},
		}
	}
};

export default config;
