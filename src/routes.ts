import { Audio, CatchAll, Home, Image, Location, Motion, Video } from './pages';

export const routes = {
  '/': Home,
  '/video': Video,
  '/audio': Audio,
  '/image': Image,
  '/motion': Motion,
  '/location': Location,
  '*': CatchAll
};
