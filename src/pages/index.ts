import Audio from '@/pages/audio.svelte';
import CatchAll from '@/pages/catchall.svelte';
import Home from '@/pages/home.svelte';
import Image from '@/pages/image.svelte';
import Location from '@/pages/location.svelte';
import Motion from '@/pages/motion.svelte';
import Video from '@/pages/video.svelte';

export { Audio, CatchAll, Home, Image, Location, Motion, Video };
