<script lang="ts">
  import { dev } from '$app/environment';
  import { base } from '$app/paths';
  import { onDestroy, onMount } from 'svelte';
  import { MetaTags } from 'svelte-meta-tags';
  import { replace } from 'svelte-spa-router';

  import Modal from '@/components/modal.svelte';

  import { download, getPlatform, iCanHasMotion, isApple, noOp, randomFileName } from '@/utils';

  import type { MotionSample } from '@/types/sample';

  let constraintMode: 'seconds' | 'samples' | null = 'seconds';
  let error: string | null = null;
  let started = false;
  let timeStarted = 0;
  let collecting = false;
  const minDurationValue = 5;
  const maxDurationValue = 60;
  const minSamplesValue = 100;
  const maxSamplesValue = 10000;
  let maxDuration = 5;
  let maxSamples = 200;
  let sampleValues: MotionSample[] = [];
  let userPermission = 'unknown';
  let editingConstraint = false;
  let iCanHasSubmitMotion = false;
  let iCanHasUpDownMotion = false;

  /**
   * Check if we should stop motion collection.
   * @returns {boolean} - Whether we should stop.
   */
  const shouldStop = (): boolean => {
    if (constraintMode === 'seconds') {
      return new Date().getTime() - timeStarted > maxDuration * 1000;
    }
    if (constraintMode === 'samples') {
      return sampleValues.length >= maxSamples;
    }
    if (sampleValues.length >= maxSamplesValue) {
      return true;
    }
    if (timeStarted && new Date().getTime() - timeStarted > maxDurationValue * 1000) {
      return true;
    }
    return false;
  };
  /**
   * Get the elapsed time.
   * @returns {number} The elapsed time in seconds.
   */
  const getElapsedTime = (): number => {
    if (started) {
      const now = new Date().getTime();
      return Math.round((now - timeStarted) / 1000);
    }
    return 0;
  };
  /**
   * Handle motion event.
   * @param {DeviceMotionEvent} event - The motion event.
   * @returns {void}
   */
  const onMotionEvent = (event: DeviceMotionEvent): void => {
    if (collecting) {
      if (!started) {
        started = true;
        timeStarted = new Date().getTime();
      }
      if (shouldStop()) {
        stopMotion();
      } else {
        sampleValues.push({
          timestamp: new Date().getTime() / 1000, // in python: datetime.fromtimestamp(timestamp)
          accelerationIncludingGravity: event.accelerationIncludingGravity,
          acceleration: event.acceleration,
          rotationRate: event.rotationRate,
          interval: event.interval
        });
        const progressEl = document.querySelector('#motion-progress');
        if (progressEl) {
          const elapsedTime = getElapsedTime();
          progressEl.innerHTML =
            'Collected ' +
            sampleValues.length +
            ' samples. Elapsed time: ' +
            elapsedTime +
            ' second' +
            (elapsedTime === 1 ? '&nbsp;' : 's');
        }
      }
    }
  };
  /**
   * On iOS, we need to ask for permission to access the motion sensor.
   * @param {Function} cb - The callback.
   */
  const checkPermission = (cb: (canStart: boolean) => void): void => {
    if (isApple() && userPermission === 'unknown') {
      /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
      // @ts-ignore
      if (typeof DeviceMotionEvent.requestPermission === 'function') {
        /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
        // @ts-ignore
        DeviceMotionEvent.requestPermission()
          .then((response: 'granted' | 'denied' | 'default') => {
            if (response === 'granted') {
              userPermission = 'granted';
              cb(true);
            } else {
              error = 'No permission to access motion sensor';
              cb(false);
            }
          })
          .catch(() => {
            error = 'No permission to access motion sensor';
            cb(false);
          });
      } else {
        cb(true);
      }
    } else {
      cb(true);
    }
  };
  /**
   * Start motion collection.
   * @returns {void}
   */
  const startMotion = (): void => {
    if (!collecting) {
      collecting = true;
      iCanHasUpDownMotion = false;
      checkPermission((canStart: boolean) => {
        if (canStart) {
          started = false;
          sampleValues = [];
        } else {
          collecting = false;
        }
      });
    }
  };
  /**
   * Stop motion collection.
   * @returns {void}
   */
  const stopMotion = (): void => {
    if (collecting) {
      collecting = false;
    }
    iCanHasUpDownMotion = sampleValues.length > minSamplesValue;
  };
  /**
   * Update the constraints based on the current values.
   * @returns {void}
   */
  const updateConstraints = (): void => {
    if (!collecting) {
      const constrainModelSelect: HTMLSelectElement | null =
        document.querySelector('#motion-constraint-mode');
      if (constrainModelSelect) {
        const motionConstraintValue: HTMLInputElement | null = document.querySelector(
          '#motion-constraint-value'
        );
        if (motionConstraintValue) {
          if (editingConstraint) {
            motionConstraintValue.blur();
          } else {
            motionConstraintValue.focus();
          }
          if (constrainModelSelect.value === 'seconds') {
            constraintMode = 'seconds';
            if (motionConstraintValue.value === '') {
              motionConstraintValue.value = maxDuration.toString();
            }
            if (motionConstraintValue.value !== '') {
              const _maxDuration = parseInt(motionConstraintValue.value, 10);
              if (_maxDuration >= minDurationValue && _maxDuration <= maxDurationValue) {
                maxDuration = _maxDuration;
              } else {
                maxDuration = minDurationValue;
              }
            }
          } else {
            constraintMode = 'samples';
            if (motionConstraintValue.value === '') {
              motionConstraintValue.value = maxSamples.toString();
            }
            if (motionConstraintValue.value !== '') {
              const _maxSamples = parseInt(motionConstraintValue.value, 10);
              if (_maxSamples >= minSamplesValue && _maxSamples <= maxSamplesValue) {
                maxSamples = _maxSamples;
              } else {
                maxSamples = minSamplesValue;
              }
            }
          }
        }
      }
      editingConstraint = !editingConstraint;
    }
  };
  /**
   * Toggle the modal visibility.
   */
  const toggleModal = () => {
    iCanHasSubmitMotion = !iCanHasSubmitMotion;
  };
  /**
   * Convert the sample values to a json string.
   * @returns {str} The json string.
   */
  const toJson = (): string => {
    const keys: string[] = [
      'timestamp',
      'accelerationIncludingGravity.x',
      'accelerationIncludingGravity.y',
      'accelerationIncludingGravity.z',
      'acceleration.x',
      'acceleration.y',
      'acceleration.z',
      'rotationRate.alpha',
      'rotationRate.beta',
      'rotationRate.gamma',
      'interval'
    ];
    const values: (number | null)[] = [];
    for (let index = 0; index < sampleValues.length; index++) {
      const sample = sampleValues[index];
      values.push(sample.timestamp);
      values.push(
        sample.accelerationIncludingGravity ? sample.accelerationIncludingGravity.x : null
      );
      values.push(
        sample.accelerationIncludingGravity ? sample.accelerationIncludingGravity.y : null
      );
      values.push(
        sample.accelerationIncludingGravity ? sample.accelerationIncludingGravity.z : null
      );
      values.push(sample.acceleration ? sample.acceleration.x : null);
      values.push(sample.acceleration ? sample.acceleration.y : null);
      values.push(sample.acceleration ? sample.acceleration.z : null);
      values.push(sample.rotationRate ? sample.rotationRate.alpha : null);
      values.push(sample.rotationRate ? sample.rotationRate.beta : null);
      values.push(sample.rotationRate ? sample.rotationRate.gamma : null);
      values.push(sample.interval);
    }
    const data = JSON.stringify({
      keys,
      values,
      meta: {
        platform: getPlatform(),
        timeStarted: timeStarted / 1000, // in python: datetime.fromtimestamp(timestamp)
        labels: {
          'accelerationIncludingGravity.x': 'accX',
          'accelerationIncludingGravity.y': 'accY',
          'accelerationIncludingGravity.z': 'accZ',
          'rotationRate.alpha': 'rotAlpha',
          'rotationRate.beta': 'rotBeta',
          'rotationRate.gamma': 'rotGamma',
          timestamp: 'timestamp'
        }
        // meta for plots: skip: ['acceleration.x', 'acceleration.y', 'acceleration.z'],
        // only use accelerationIncludingGravity instead
        // don't use the timestamp in the plot
      }
    });
    return data;
  };
  /**
   * Convert the sample values to a json file.
   * @returns {File} The json file.
   */
  const toFile = (): File => {
    const data = toJson();
    const motionBlob = new Blob([data], { type: 'application/json' });
    const filename = randomFileName('motion', 'json');
    return new File([motionBlob], filename, { type: 'application/json' });
  };
  /**
   * Download the sample values as a json file.
   * @returns {void}
   */
  const _download = (): void => {
    const data = toJson();
    const motionBlob = new Blob([data], { type: 'application/json' });
    const filename = randomFileName('motion', 'json');
    download(motionBlob, filename);
  };
  /**
   * Onmount hook. Check if the device supports the DeviceMotion API.
   * @returns {void}
   */
  onMount(() => {
    if (!iCanHasMotion()) {
      error = 'Your browser does not support the DeviceMotion API.';
    } else {
      window.addEventListener('devicemotion', onMotionEvent);
    }
  });
  /**
   * Ondestroy hook. Stop the motion and reset the values.
   * @returns {void}
   */
  onDestroy(() => {
    stopMotion();
    constraintMode = 'seconds';
    error = null;
    started = false;
    timeStarted = 0;
    collecting = false;
    maxDuration = 5;
    maxSamples = 200;
    sampleValues = [];
    editingConstraint = false;
    iCanHasSubmitMotion = false;
    iCanHasUpDownMotion = false;
    window.removeEventListener('devicemotion', onMotionEvent);
  });
</script>

<MetaTags
  title="Collect - Motion"
  description="Collect accelerometer and gyroscope samples from your device."
/>
<div>
  <h1>Motion</h1>
  <h2 style="width: 100vw;text-align:center">
    <a href="{base}/#/" on:click={() => replace(`${base}/#/`)}>Home</a>
  </h2>
  <div>
    {#if error}
      <div class="error">{error}</div>
    {/if}
    {#if iCanHasMotion() || dev}
      <div class="motion-wrapper">
        <div class="motion-options">
          <label for="motion-constraint-mode" style="margin-right: 10px;">Stop at: </label>
          <div class="motion-constraint-value">
            {#if constraintMode === 'seconds'}
              <input
                type="number"
                inputmode="numeric"
                pattern="[0-9]*"
                value={maxDuration}
                min={minDurationValue}
                max={maxDurationValue}
                disabled={!editingConstraint}
                readonly={!editingConstraint}
                style="max-width: 30vw;"
                id="motion-constraint-value"
              />
            {:else if constraintMode === 'samples'}
              <input
                type="number"
                inputmode="numeric"
                pattern="[0-9]*"
                value={maxSamples}
                min={minSamplesValue}
                max={maxSamplesValue}
                disabled={!editingConstraint}
                readonly={!editingConstraint}
                style="max-width: 30vw;"
                id="motion-constraint-value"
              />
            {/if}
          </div>
          <select
            bind:value={constraintMode}
            disabled={!editingConstraint}
            id="motion-constraint-mode"
            style="max-width: 40vw;"
          >
            <option value={null} disabled>Select</option>
            <option value={null}>No constraint</option>
            <option value="seconds">Seconds</option>
            <option value="samples">Samples</option>
          </select>
          <div style="margin: 0 5px 0;padding: 0 5px">
            <i
              class="fa fa-lg fa-solid {editingConstraint ? 'fa-check' : 'fa-edit'}"
              on:click={updateConstraints}
              on:keydown={noOp}
              id="motion-constraint-edit"
              role="button"
              tabindex="0"
            />
          </div>
        </div>
        <div class="motion-actions">
          <i
            class="fa-2x fa-solid fa-play-circle"
            style="padding: 5px;display: {collecting ? 'none' : 'initial'}"
            id="motion-record"
            role="button"
            tabindex="0"
            on:click={startMotion}
            on:keydown={noOp}
          />
          <i
            class="fa-2x fa-solid fa-stop-circle"
            style="padding: 5px;display: {collecting ? 'initial' : 'none'}"
            id="motion-stop"
            role="button"
            tabindex="0"
            on:click={stopMotion}
            on:keydown={noOp}
          />
          <i
            class="fa-2x fa-solid fa-download"
            style="display: {iCanHasUpDownMotion ? 'initial' : 'none'};margin-left: 40px"
            id="motion-download"
            role="button"
            tabindex="0"
            on:click={_download}
            on:keydown={noOp}
          />
          <i
            class="fa-2x fa-solid fa-upload"
            style="display: {iCanHasUpDownMotion ? 'initial' : 'none'};margin-left: 40px"
            id="motion-upload"
            role="button"
            tabindex="0"
            on:click={toggleModal}
            on:keydown={noOp}
          />
        </div>
        <div id="motion-progress" />
      </div>
    {/if}
    {#if iCanHasSubmitMotion}
      <Modal on:click={toggleModal} file={toFile()} />
    {/if}
  </div>
</div>

<style>
  .motion-actions,
  .motion-options {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 10px auto 0;
    height: 50px;
  }
  .motion-wrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
</style>
