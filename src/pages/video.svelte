<script lang="ts">
  import { base } from '$app/paths';
  import { onDestroy, onMount } from 'svelte';
  import { MetaTags } from 'svelte-meta-tags';
  import { replace } from 'svelte-spa-router';

  import Modal from '@/components/modal.svelte';

  import {
    download,
    getMediaRecorderAndStream,
    iCanHasMediaDevices,
    iCanHasFacingModeConstraints,
    noOp,
    randomFileName,
    videoExtension,
    videoType
  } from '@/utils';

  let videoChunks: Blob[] = [];
  let isRecordingVideo = false;
  let videoStream: MediaStream | null = null;
  let videoRecorder: MediaRecorder | null = null;
  let error: string | null = null;
  let videoFacingMode: 'user' | 'environment' = 'user';
  let canChangeFacingMode = false;
  let checkedForFacingMode = false;
  let iCanHasUpDownVideo = false;
  let iCanHasSubmitVideo = false;
  /**
   * Start recording video.
   * @returns {void}
   */
  const startRecording = (): void => {
    if (!isRecordingVideo) {
      videoChunks = [];
      if (videoRecorder) {
        videoRecorder.start();
      }
      const videoEl: HTMLVideoElement | null = document.querySelector('#video');
      if (videoEl && videoStream) {
        videoEl.pause();
        videoEl.srcObject = videoStream;
        videoEl.muted = true;
        videoEl.play();
        videoEl.controls = false;
      }
      isRecordingVideo = true;
    }
  };
  /**
   * Stop recording video.
   * @returns {void}
   */
  const stopRecording = (): void => {
    if (isRecordingVideo) {
      if (videoRecorder) {
        videoRecorder.stop();
      }
      const videoEl: HTMLVideoElement | null = document.querySelector('#video');
      if (videoEl) {
        videoEl.pause();
        videoEl.muted = false;
        videoEl.controls = true;
        videoEl.srcObject = null;
      }
      isRecordingVideo = false;
    }
  };
  /**
   * Handle video recording data event.
   * @param {BlobEvent} event - The event object.
   * @returns {void}
   */
  const onVideoRecordingDataEvent = (event: BlobEvent): void => {
    if (event.data.size > 0) {
      videoChunks.push(event.data);
    }
    const videoEl: HTMLVideoElement | null = document.querySelector('#video');
    if (
      videoEl &&
      videoRecorder &&
      videoRecorder.state === 'inactive' &&
      !isRecordingVideo &&
      videoChunks.length > 0
    ) {
      const videoBlob = new Blob(videoChunks, { type: videoType() });
      videoEl.src = URL.createObjectURL(videoBlob);
      iCanHasUpDownVideo = true;
    } else {
      iCanHasUpDownVideo = false;
    }
  };
  /**
   * Flip the camera from front to back and vice versa
   * @returns {void}
   */
  const onFlipImageCamera = (): void => {
    if (!isRecordingVideo) {
      videoFacingMode = videoFacingMode === 'environment' ? 'user' : 'environment';
      stopVideoStream();
      startVideoStream();
    }
  };
  /**
   * Stop the video stream
   * @returns {void}
   */
  const stopVideoStream = (): void => {
    if (videoStream) {
      const tracks = videoStream.getTracks();
      tracks.forEach((track) => track.stop());
    }
    if (videoRecorder) {
      videoRecorder.removeEventListener('dataavailable', onVideoRecordingDataEvent);
    }
  };
  /**
   * Start the video stream
   * @returns {void}
   */
  const startVideoStream = (): void => {
    getMediaRecorderAndStream(true, true, videoFacingMode, (result) => {
      if (!result) {
        error =
          'Your browser does not support the MediaRecorder API,or no permission to use the camera and the microphone is granted';
        return;
      }
      if (!checkedForFacingMode) {
        // no permission (yet) on previous call?
        iCanHasFacingModeConstraints((result) => {
          canChangeFacingMode = result;
        });
      }
      videoRecorder = result.recorder;
      videoStream = result.stream;
      videoRecorder.addEventListener('dataavailable', onVideoRecordingDataEvent);
      if (videoStream) {
        const videoEl = document.querySelector('video');
        if (videoEl) {
          videoEl.srcObject = videoStream;
          videoEl.controls = false;
          videoEl.muted = true;
          videoEl.play().catch(noOp);
        }
      }
    });
  };
  /**
   * Convert the video chunks to a file
   * @returns {File} The video file
   */
  const toFile = (): File => {
    const videoBlob = new Blob(videoChunks, { type: videoType() });
    const filename = randomFileName('video', videoExtension());
    const videoFile = new File([videoBlob], filename, { type: videoType() });
    return videoFile;
  };
  /**
   * Download the video file
   * @returns {void}
   */
  const _download = () => {
    const videoBlob = new Blob(videoChunks, { type: videoType() });
    const filename = randomFileName('video', videoExtension());
    download(videoBlob, filename);
  };
  /**
   * Toggle the modal visibility
   * @returns {void}
   */
  const toggleModal = () => {
    iCanHasSubmitVideo = !iCanHasSubmitVideo;
  };
  /**
   * Handle orientation change event.
   * @returns {void}
   */
  const onOrientationChange = (): void => {
    setTimeout(() => {
      stopVideoStream();
      startVideoStream();
    }, 100);
  };
  /**
   * On mount hook.
   * @returns {void}
   */
  onMount(() => {
    setTimeout(() => {
      if (!iCanHasMediaDevices()) {
        error = 'Your browser does not support the MediaRecorder API';
      }
      iCanHasFacingModeConstraints((itDoes) => {
        canChangeFacingMode = itDoes;
      });

      startVideoStream();
      window.addEventListener('orientationchange', onOrientationChange);
    }, 200);
  });
  /**
   * On destroy hook.
   * @returns {void}
   */
  onDestroy(() => {
    stopVideoStream();
    videoChunks = [];
    isRecordingVideo = false;
    videoStream = null;
    videoRecorder = null;
    error = null;
    iCanHasUpDownVideo = false;
    iCanHasSubmitVideo = false;
    window.removeEventListener('orientationchange', onOrientationChange);
  });
</script>

<MetaTags title="Collect - Video" description="Collect video recordings from your device." />
<div>
  <h1>Video</h1>
  <h2 style="width: 100vw;text-align:center">
    <a href="{base}/#/" on:click={() => replace(`${base}/#/`)}>Home</a>
  </h2>
  <div>
    {#if error}
      <div class="error">{error}</div>
    {:else if iCanHasMediaDevices()}
      <div class="video-actions">
        {#if canChangeFacingMode}
          <i
            class="fa-2x fa-solid fa-camera-rotate"
            style="margin-right:10px;padding: 5px;display: {isRecordingVideo ? 'none' : 'initial'}"
            id="video-change-camera"
            aria-roledescription="Toggle camera facing mode"
            role="button"
            tabindex="0"
            on:click={onFlipImageCamera}
            on:keydown={noOp}
          />
        {/if}
        <i
          class="fa-2x fa-solid fa-video-camera"
          style="padding: 5px;display: {isRecordingVideo || !videoStream ? 'none' : 'initial'}"
          id="video-record"
          aria-roledescription="Start recording"
          role="button"
          tabindex="0"
          on:click={startRecording}
          on:keydown={noOp}
        />
        <i
          class="fa-2x fa-solid fa-stop-circle"
          style="padding: 5px;display: {isRecordingVideo ? 'initial' : 'none'}"
          id="video-stop"
          aria-roledescription="Sop recording"
          role="button"
          tabindex="0"
          on:click={stopRecording}
          on:keydown={noOp}
        />
        <i
          class="fa-2x fa-solid fa-download"
          style="display: {iCanHasUpDownVideo ? 'initial' : 'none'};margin-left: 40px"
          id="video-download"
          aria-roledescription="Download recording"
          role="button"
          tabindex="0"
          on:click={_download}
          on:keydown={noOp}
        />
        <i
          class="fa-2x fa-solid fa-upload"
          style="display: {iCanHasUpDownVideo ? 'initial' : 'none'};margin-left: 40px"
          id="video-upload"
          aria-roledescription="Upload recording"
          role="button"
          tabindex="0"
          on:click={toggleModal}
          on:keydown={noOp}
        />
      </div>
      <div class="video-wrapper">
        <!-- svelte-ignore a11y-media-has-caption -->
        <video id="video" width="640" height="480" playsinline />
      </div>
      {#if iCanHasSubmitVideo}
        <Modal on:click={toggleModal} file={toFile()} />
      {/if}
    {/if}
  </div>
</div>

<style>
  .video-actions {
    display: flex;
    align-items: center;
    justify-content: center;
    max-width: 100px;
    margin: 20px auto;
  }
  .video-wrapper {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .video-wrapper video {
    width: 100%;
    max-width: 640px;
  }
</style>
