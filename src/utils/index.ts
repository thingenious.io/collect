import { browser } from '$app/environment';
import { getUUID } from './uuid';

export const noOp = () => null;
export * from './location';
export * from './media';
export * from './motion';
export * from './platform';
export * from './theme';
export * from './uuid';

/**
 * Wait for the DOM to be ready
 * @param then Once the DOM is ready, this function will be called
 */
export const ready = (then: () => void) => {
  if (browser) {
    if (['interactive', 'complete'].includes(document.readyState)) {
      then();
    } else {
      document.addEventListener('DOMContentLoaded', then);
    }
  } else {
    then();
  }
};

/**
 * Download the sample data as a file.
 * @param sample The sample data to download
 * @param filename The name of the file to download
 */
export const download = (sample: Blob, filename: string) => {
  const a = document.createElement('a');
  a.href = URL.createObjectURL(sample);
  a.download = filename;
  const url = a.href;
  const clickHandler = () => {
    setTimeout(() => {
      URL.revokeObjectURL(url);
      a.removeEventListener('click', clickHandler);
    }, 150);
  };
  a.addEventListener('click', clickHandler, false);
  a.click();
};

/**
 * Upload a file to a server.
 * @param {File?} file The file to upload if any
 * @param {Record<String, String>?} extras additional details to send with the request
 * @param {string} url The URL to upload the file to
 * @param {Record<string,string>} headers The headers to send with the request
 * @param {string|null} contentType The content type of the file. This is usually 'multipart/form-data' but in some cases needs to be omitted.
 * @returns {Promise<{success: boolean, message: string | null}>} A promise that resolves to an object with a success boolean and the response
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
 * @see https://developer.mozilla.org/en-US/docs/Web/API/FormData
 * @see https://developer.mozilla.org/en-US/docs/Web/API/AbortController
 * @see https://stackoverflow.com/a/39281156
 */
export const upload = (
  url: string,
  headers: Record<string, string>,
  file: File | null = null,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  extras: Record<string, any> | null = null,
  contentType: string | null = null
): Promise<{ success: boolean; message: string | null }> => {
  return new Promise((resolve) => {
    if ((file === null || file == undefined) && (extras === null || extras == undefined)) {
      resolve({ success: false, message: 'No file or extras provided' });
      return;
    }
    const uuid = getUUID();
    const formData = new FormData();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const jsonData: Record<string, any> = extras ? extras : {};
    if (file) {
      formData.append('id', uuid);
      formData.append('file', file);
      if (extras) {
        Object.keys(extras).forEach((key) => {
          formData.append(key, extras[key]);
        });
      }
    }
    if (contentType === 'application/json') {
      jsonData['id'] = uuid;
    }
    const fetchBody = contentType === 'application/json' ? JSON.stringify(jsonData) : formData;
    const abortController = new AbortController();
    const timeoutId = setTimeout(() => abortController.abort(), 10000);
    fetch(url, {
      method: 'POST',
      headers: {
        ...headers,
        ...(contentType ? { 'Content-Type': contentType } : {})
      },
      body: fetchBody,
      signal: abortController.signal
    })
      .then((response) => {
        clearTimeout(timeoutId);
        if (response.ok) {
          resolve({ success: true, message: null });
        }
        resolve({ success: false, message: null });
      })
      .catch((error) => {
        clearTimeout(timeoutId);
        if (error.message) {
          resolve({ success: false, message: error.message });
        } else {
          let message = '';
          if (error.name === 'AbortError') {
            message = 'The request was aborted';
          } else {
            message = 'Status: ' + error.status + ' - ' + error.statusText + '';
          }
          resolve({ success: false, message });
        }
      });
  });
};
/**
 * Generate a random string
 * @param prefix {string} The prefix to use for the random name
 * @param extension {string} The extension to use for the random name
 * @returns {string} A random name
 */
export const randomFileName = (prefix: string, extension: string): string => {
  const common = new Date().getTime();
  const random = Math.random() * 100;
  return `${prefix}-${common}-${random.toString().replace('.', '')}.${extension}`;
};

/**
 * Convert a canvas to a blob
 * @param {HTMLCanvasElement}  canvas The canvas to convert
 * @param {string} type The type of the blob
 * @returns {Blob} The blob.
 * @see credits: https://stackoverflow.com/questions/23150333/html5-javascript-dataurl-to-blob-blob-to-dataurl/30407959#30407959
 */
export const canvasToBlob = (canvas: HTMLCanvasElement, type: string): Blob => {
  const dataURL = canvas.toDataURL(type);
  const base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  const binary = window.atob(base64);
  const array = [];
  for (let i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type });
};
