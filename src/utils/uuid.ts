/**
 * Get a UUID stored in localStorage or create a new one.
 * @returns {string} UUID
 */
export const getUUID = (): string => {
  let uuid = localStorage.getItem('collection-uuid');
  if (!uuid) {
    uuid = crypto.randomUUID();
    localStorage.setItem('collection-uuid', uuid);
  }
  return uuid;
};
