import { isAndroid } from './platform';

/**
 *  Checks if the device has motion sensors
 * @returns {boolean} true if the device has motion sensors
 */
export const iCanHasMotion = (): boolean => {
  // `requestPermission` is only available on iOS
  // https://developer.mozilla.org/en-US/docs/Web/API/DeviceMotionEvent
  if (typeof window.DeviceMotionEvent !== 'undefined') {
    //eslint-disable-next-line
    // @ts-ignore
    if (typeof window.DeviceMotionEvent.requestPermission === 'function') {
      return true;
    }
    //on android, the `requestPermission` method might not be available.
    return isAndroid();
  }
  return false;
};
