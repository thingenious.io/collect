import { browser } from '$app/environment';

import { isApple } from '@/utils/platform';

/**
 * Check if we have `navigator.mediaDevices`
 * @returns {boolean} True if we do, false otherwise
 */
export const iCanHasMediaDevices = (): boolean => {
  return !(!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices);
};
/**
 * Get video/audio media recorder and stream.
 * @param video: {boolean} Check for video device.
 * @param audio: {boolean} Check for audio device.
 * @param facingMode: {string} either 'user' or 'environment', for video device.
 * @param callback {function} What to do with the result.
 */
export const getMediaRecorderAndStream = (
  video: boolean,
  audio: boolean,
  facingMode: 'user' | 'environment',
  callback: (result: { recorder: MediaRecorder; stream: MediaStream } | null) => void
) => {
  if (iCanHasMediaDevices() && browser) {
    const width = window.innerHeight > window.innerWidth ? 480 : 640;
    const height = window.innerHeight > window.innerWidth ? 640 : 480;
    const constraints = {
      video: video
        ? {
            width,
            height,
            facingMode: facingMode
          }
        : false,
      audio: audio
        ? {
            sampleRate: 48000,
            channelCount: 2,
            noiseSuppression: true,
            autoGainControl: true,
            echoCancellation: true,
            volume: 1.0
          }
        : false
    };
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then((stream) => {
        const recorder = new MediaRecorder(stream);
        callback({ recorder, stream });
      })
      .catch((err) => {
        console.error(err);
        callback(null);
      });
  } else {
    callback(null);
  }
};
/**
 * Check if we have more than one `videoinput` devices. i.e. two cameras (for facing mode)
 * @param callback
 */
export const _iCanHasMoreThanOneCamera = (callback: (itDoes: boolean) => void) => {
  if (iCanHasMediaDevices()) {
    navigator.mediaDevices
      .enumerateDevices()
      .then((devices) => {
        const videoDevices = devices.filter((device) => device.kind === 'videoinput');
        callback(videoDevices.length > 1);
      })
      .catch((err) => {
        alert(err.message);
        console.error(err);
        callback(false);
      });
  } else {
    callback(false);
  }
};

/**
 * Check we can switch the facing mode from user to environment and back.
 * @param callback
 */
export const iCanHasFacingModeConstraints = (callback: (idDoes: boolean) => void) => {
  _iCanHasMoreThanOneCamera((isDoes) => {
    if (!isDoes) {
      callback(false);
    } else {
      const supportedConstraints = navigator.mediaDevices.getSupportedConstraints();
      for (const constraint of Object.keys(supportedConstraints)) {
        if (constraint.toLowerCase() === 'facingmode') {
          callback(true);
          break;
        }
      }
      callback(false);
    }
  });
};

/**
 * Get the file extension to use for audio recording.
 * @returns {string} .mp4 if not iOS, .webm otherwise.
 */
export const audioExtension = (): string => {
  return isApple() ? 'mp4' : 'webm';
};
/**
 * Get the audio mime type.
 * @returns {string} `audio/{mpeg,webm}`
 */
export const audioType = (): string => {
  return isApple() ? 'audio/mpeg' : 'audio/webm';
};
/**
 * Get the file extension to use for video recording.
 * @returns {string} .mp4 is not iOS, .webm otherwise.
 */
export const videoExtension = (): string => {
  return isApple() ? 'mp4' : 'webm';
};
/**
 * Get the video mime type.
 * @returns {string} `video/{mp4,webm}`
 */
export const videoType = (): string => {
  return isApple() ? 'video/mp4' : 'video/webm';
};
