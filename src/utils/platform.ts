// Description: Platform detection

/**
 * Detects the platform of the device
 * @returns {string} the platform of the device
 */
export const getPlatform = (): string => {
  if (typeof navigator === 'undefined') {
    return 'unknown';
  }
  if (Object.prototype.hasOwnProperty.call(navigator, 'userAgentData')) {
    // https://developer.mozilla.org/en-US/docs/Web/API/NavigatorUAData/platform
    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
    // @ts-ignore
    return navigator.userAgentData.platform;
  }
  return navigator?.platform || 'unknown';
};

/**
 * Detects if the device is an iOS device
 * @returns {boolean} true if the device is an iOS device
 */
export const isIOS = (): boolean => {
  return (
    ['iPad Simulator', 'iPhone Simulator', 'iPod Simulator', 'iPad', 'iPhone', 'iPod'].includes(
      getPlatform()
    ) ||
    // iPad on iOS 13 detection
    (navigator.userAgent.includes('Mac') && 'ontouchend' in document)
  );
};

/**
 * Detects if the browser is Safari
 * @returns {boolean} true if the browser is Safari
 */
export const isSafari = (): boolean => {
  return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
};

/**
 * Detects if the device is an iOS device
 * @returns {boolean} true if the device is an iOS device
 * @see isIOS
 * @see isSafari
 */
export const isApple = (): boolean => {
  if (isIOS() || isSafari()) {
    return true;
  }
  return navigator.userAgent.toLowerCase().indexOf('mac') > -1;
};

/**
 * Detects if the device is an Android device
 * @returns {boolean} true if the device is an Android device
 */
export const isAndroid = (): boolean => {
  return navigator.userAgent.toLowerCase().indexOf('android') > -1;
};

/**
 * Detects if the device is a mobile device
 * @returns {boolean} true if the device is a mobile device
 * @see isIOS
 * @see isAndroid
 */
export const isMobile = (): boolean => {
  return isIOS() || isAndroid();
};
