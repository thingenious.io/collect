/**
 * Checks if the device is in dark mode
 * @returns {boolean} true if the device is in dark mode
 */
export function isDarkMode(): boolean {
  const jsonEntry = window.localStorage.getItem('themeMode');
  let entry = null;
  if (jsonEntry) {
    try {
      entry = JSON.parse(jsonEntry);
    } catch (e) {
      entry = null;
    }
  }
  if (entry !== undefined && entry !== null && 'theme' in entry) {
    return entry.theme === 'dark';
  }
  return window.matchMedia('(prefers-color-scheme: dark)').matches;
}
/**
 * Updates the HTML head to match the theme
 * @param darkMode {boolean} true if the theme is dark
 * @returns {void}
 */
export function updateHtmlHead(darkMode: boolean): void {
  document
    .querySelector('meta[name="theme-color"]')
    ?.setAttribute('content', darkMode ? '#000000' : '#ffffff');
  document
    .querySelector('meta[name="msapplication-TileColor"]')
    ?.setAttribute('content', darkMode ? '#000000' : '#ffffff');
  document.querySelector('body')?.setAttribute('class', darkMode ? 'theme-dark' : 'theme-light');
  const current = darkMode ? 'dark' : 'light';
  const replacement = darkMode ? 'light' : 'dark';
  const currentFaviconElements = document.querySelectorAll('link[rel="shortcut icon"]');
  if (currentFaviconElements) {
    currentFaviconElements.forEach((element) => {
      if (element.getAttribute('href')) {
        const currentFaviconHref: string | null = element.getAttribute('href');
        if (currentFaviconHref) {
          element.setAttribute('href', currentFaviconHref.replace(replacement, current));
        }
      }
    });
  }
  const maskIcon = document.querySelector('link[rel="mask-icon"]');
  if (maskIcon) {
    const currentMaskIconHref: string | null = maskIcon.getAttribute('href');
    if (currentMaskIconHref) {
      maskIcon.setAttribute('href', currentMaskIconHref.replace(replacement, current));
    }
    maskIcon.setAttribute('color', darkMode ? '#000000' : '#ffffff');
  }
  const appleTouchIcon = document.querySelector('link[rel="apple-touch-icon"]');
  if (appleTouchIcon) {
    const currentAppleTouchIconHref: string | null = appleTouchIcon.getAttribute('href');
    if (currentAppleTouchIconHref) {
      appleTouchIcon.setAttribute('href', currentAppleTouchIconHref.replace(replacement, current));
    }
  }
  const msApplicationConfig = document.querySelector('meta[name="msapplication-config"]');
  if (msApplicationConfig) {
    const currentContent: string | null = msApplicationConfig.getAttribute('content');
    if (currentContent) {
      msApplicationConfig.setAttribute('content', currentContent.replace(replacement, current));
    }
  }
  const manifest: HTMLElement | null = document.querySelector('link[rel="manifest"]');
  if (manifest) {
    const currentManifestHref: string | null = manifest.getAttribute('href');
    if (currentManifestHref) {
      manifest.setAttribute('href', currentManifestHref.replace(replacement, current));
    }
  }
  const currentLogoElement: HTMLElement | null = document.querySelector(`#app-logo-${replacement}`);
  const otherLogoElement: HTMLElement | null = document.querySelector(`#app-logo-${current}`);
  if (currentLogoElement && otherLogoElement) {
    currentLogoElement.style.display = 'none';
    otherLogoElement.style.display = 'unset';
  }
}
/**
 * Updates the theme toggle button to match the theme
 * @param darkMode {boolean} true if the theme is dark
 * @returns {void}
 */
export function updateToggleButton(darkMode: boolean): void {
  const selectorOne = darkMode ? '#toggle-icon-dark' : '#toggle-icon-light';
  const selectorTwo = darkMode ? '#toggle-icon-light' : '#toggle-icon-dark';
  const iconOne: HTMLElement | null = document.querySelector(selectorOne);
  const iconTwo: HTMLElement | null = document.querySelector(selectorTwo);
  if (iconOne && iconTwo) {
    iconOne.setAttribute('style', 'background-color: currentcolor');
    iconTwo.setAttribute('style', 'background-color: transparent');
  }
}
/**
 * Toggles the theme from light to dark and vice versa
 * @returns {void}
 */
export function toggleTheme(): void {
  if (!window.localStorage.getItem('theme.lock')) {
    window.localStorage.setItem('theme.lock', '1');
    const isDark = isDarkMode();
    document.querySelector('body')?.setAttribute('class', isDark ? 'theme-dark' : 'theme-light');
    window.localStorage.setItem('themeMode', JSON.stringify({ theme: isDark ? 'light' : 'dark' }));
    updateHtmlHead(!isDark);
    updateToggleButton(!isDark);
    setTimeout(() => {
      window.localStorage.removeItem('theme.lock');
    }, 200);
  }
}
