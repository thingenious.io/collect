/**
 * Check if we have navigator.geolocation.
 */
export const iCanHasGeoLocation = (): boolean => {
  return (
    'geolocation' in navigator &&
    navigator.geolocation !== undefined &&
    navigator.geolocation !== null
  );
};
