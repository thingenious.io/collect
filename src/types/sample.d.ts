export type MotionSample = {
  timestamp: number;
  accelerationIncludingGravity: DeviceMotionEventAcceleration | null;
  acceleration: DeviceMotionEventAcceleration | null;
  rotationRate: DeviceMotionEventRotationRate | null;
  interval: number;
};
