# Collect

...ed tools to capture image, audio, video, location (and motion[accelerometer/acceleration rate] if available) from a device.

Collected mostly from examples on [https://developer.mozilla.org/en-US/docs/Web/API](https://developer.mozilla.org/en-US/docs/Web/API)

Using [https://svelte.dev](https://svelte.dev)

Result: [https://thingenious.gitlab.io/collect/](https://thingenious.gitlab.io/collect/)

A simple server implementation to handle the uploaded data (and query uploaded locations) can be found in the [server](server/) directory.
