const _DARK_COLOR = '#1d1f23';
const _LIGHT_COLOR = '#fcfafe';

function isDarkMode() {
    const jsonEntry = window.localStorage.getItem("themeMode");
    let entry = null;
    if (jsonEntry) {
        try {
            entry = JSON.parse(jsonEntry);
        } catch (e) {
            entry = null;
        }
    }
    if(entry !== undefined && entry !==null && 'theme' in entry) {return entry.theme === "dark";}
    return window.matchMedia('(prefers-color-scheme: dark)').matches;
}
function updateHtmlHead(darkMode) {
    document
      .querySelector('meta[name="theme-color"]')
      ?.setAttribute('content', darkMode ? _DARK_COLOR : _LIGHT_COLOR);
    document
      .querySelector('meta[name="msapplication-TileColor"]')
      ?.setAttribute('content', darkMode ? _DARK_COLOR : _LIGHT_COLOR);
    document.querySelector('body')?.setAttribute('class', darkMode ? 'theme-dark': 'theme-light');
    const current = darkMode ? 'dark' : 'light';
    const replacement = darkMode ? 'light': 'dark';
    const currentFaviconElements = document.querySelectorAll('link[rel="shortcut icon"]');
    if (currentFaviconElements) {
        currentFaviconElements.forEach(element => {
            if (element.getAttribute('href')) {
                element.setAttribute('href', element.getAttribute('href').replace(replacement, current));
            }
        });
    }
    const maskIcon = document.querySelector('link[rel="mask-icon"]');
    if (maskIcon) {
        maskIcon.setAttribute('href', maskIcon.getAttribute('href').replace(replacement, current));
        maskIcon.setAttribute('color', darkMode ? _DARK_COLOR : _LIGHT_COLOR);
    }
    const appleTouchIcon = document.querySelector('link[rel="apple-touch-icon"]');
    if (appleTouchIcon) {
        appleTouchIcon.setAttribute(
          'href',
          appleTouchIcon.getAttribute('href').replace(replacement, current),
        );
    }
    const msApplicationConfig = document.querySelector('meta[name="msapplication-config"]');
    if (msApplicationConfig) {
        msApplicationConfig.setAttribute(
          'content',
          msApplicationConfig.getAttribute('content').replace(replacement, current),
        );
    }
    const manifest = document.querySelector('link[rel="manifest"]');
    if (manifest) {
        manifest.setAttribute('href', manifest.getAttribute('href').replace(replacement, current));
    }
    const currentLogoElement = document.querySelector(`#account-auth-logo-${replacement}`);
    const otherLogoElement = document.querySelector(`#account-auth-logo-${current}`);
    if (currentLogoElement && otherLogoElement) {
        currentLogoElement.style.display='none'
        otherLogoElement.style.display='unset'
    }
}
function updateBodyClass(darkMode) {
    document.querySelector('body')?.setAttribute('class', darkMode ? 'theme-dark': 'theme-light');
}

function updateToggleButtonIcon(darkMode) {
    /*
    <button
        id="theme-toggle"
        class="theme-toggle-button"
        onclick="_toggleTheme()"
    >
        <i class="fa fa-sun" id="dark-mode-icon" style="display: none"></i>
        <i class="fa fa-moon" id="light-mode-icon"></i>
    </button>
    */
    const darkModeIcon = document.querySelector('#dark-mode-icon');
    const lightModeIcon = document.querySelector('#light-mode-icon');
    if (darkModeIcon && lightModeIcon) {
        darkModeIcon.style.display = darkMode ? 'unset' : 'none';
        lightModeIcon.style.display = darkMode ? 'none' : 'unset';
    }
}

function setThemeMode(mode) {
    const isDark = mode === 'dark';
    window.localStorage.setItem('themeMode', JSON.stringify({theme: isDark ? 'dark' : 'light'}));
    updateHtmlHead(isDark);
    updateBodyClass(isDark);
    updateToggleButtonIcon(isDark);
}
// eslint-disable-next-line
function toggleTheme() {
    if (!window.localStorage.getItem('theme.lock')) {
        window.localStorage.setItem("theme.lock", "1");
        const isDark = isDarkMode();
        setThemeMode(isDark ? 'light' : 'dark');
        setTimeout(() => {
            window.localStorage.removeItem("theme.lock");
        }, 200);
    }
}
if (isDarkMode()) {
    setThemeMode('dark');
}
