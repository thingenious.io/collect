# Collect Server

A simple server example implementation to handle the data collection from the client.

It uses FastAPI to handle the requests and either PostgreSQL or SQLite to store the data.

A dummy authentication is included that covers Basic Auth and Bearer Token.

A location upload request example should follow the format of the [GeolocationPosition](https://developer.mozilla.org/en-US/docs/Web/API/GeolocationPosition) object. It also uses an id to identify the entry and an optional "extras" field to store additional data.

```json
{
    "id": "1234567890",
    "location": {
        "latitude": 37.7749,
        "longitude": -122.4194,
        "altitude": null,
        "accuracy": 0.0,
        "altitudeAccuracy": null,
        "heading": null,
        "speed": null
    },
    "timestamp": 1610000000000,
    "extras": {
        "foo": "bar"
    }
}
```

![Screenshot](screenshot.png)

Reading the location entries is possible via the `/api/location` GET endpoint and it uses the "limit" and "offset" query parameters to paginate the results.

```shell
curl -X GET "http://localhost:8000/location?limit=10&offset=0" \
-H  "accept: application/json" \
-H "Authorization Bearer: 1234567890"
```

![Screenshot](screenshot2.png)

## Docker / Podman

The server can be run via Docker or Podman.

```shell
podman build --platform linux/amd64 -t collect -f Containerfile .
```

To run the server with SQLite:

```shell
podman run --rm --init --platform linux/amd64 \
-e HTTP_PORT=8000 \
-e DOMAIN_NAME=localhost \
-e CLIENT_ID=client \
-e CLIENT_SECRET=secret \
-e SECRET_KEY=more-secret \
-e TRUSTED_HOSTS=localhost,example.com \
-e DB_USE_SQLITE=true \
-p 8000:8000 \
collect    
```

To use PostgreSQL, change the `DB_USE_SQLITE` environment variable to `false` and add the following environment variables (with the correct values):

```shell
-e DB_HOST=postgres \
-e DB_PORT=5432 \
-e DB_USER=postgres \
-e DB_PASSWORD=postgres \
-e DB_NAME=postgres \
```

A simple [compose.yaml](./compose.yaml) file is also included to run the server with PostgreSQL.

You might also want to use a volume for the `/home/user/server/media` directory to persist the uploaded files.
