"""Database management."""

from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)

# pylint: disable=unused-import, wildcard-import, unused-wildcard-import
# (*) wildcard to create the metadata/tables of all our models
from app.models import *  # noqa
from app.models import Base


class Connection:
    """Database connection."""

    _engine: AsyncEngine
    _session: async_sessionmaker[AsyncSession]

    @classmethod
    def from_db_url(cls, db_url: str) -> "Connection":
        """Create a connection from a db_url."""
        if not db_url:  # pragma: no cover
            raise ValueError("No db_url provided")
        engine = create_async_engine(db_url)
        async_session = async_sessionmaker(engine, expire_on_commit=False)
        instance = cls()
        instance.engine = engine
        instance.session = async_session
        return instance

    async def create_metadata(self) -> None:
        """Create metadata."""
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)

    @property
    def engine(self) -> AsyncEngine:
        """Get the engine."""
        return self._engine

    @engine.setter
    def engine(self, engine: AsyncEngine) -> None:
        """Set the engine."""
        self._engine = engine

    @property
    def session(self) -> async_sessionmaker[AsyncSession]:
        """Get the session."""
        return self._session

    @session.setter
    def session(self, session: async_sessionmaker[AsyncSession]) -> None:
        """Set the session."""
        self._session = session

    async def disconnect(self) -> None:
        """Disconnect from the database."""
        await self.engine.dispose()
