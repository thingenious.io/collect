"""Snowflake generation."""
# source/credits:
# https://github.com/vd2org/snowflake/blob/master/snowflake/snowflake.py

import datetime as dt
import os
from dataclasses import dataclass
from datetime import timedelta, tzinfo
from time import time
from typing import Optional

__all__ = ("Snowflake", "SnowflakeGenerator", "get_next_id")


NODE_INSTANCE = int(os.environ.get("NODE_INSTANCE", "1"))
MAX_TS = 0b11111111111111111111111111111111111111111
MAX_INSTANCE = 0b1111111111
MAX_SEQ = 0b111111111111


@dataclass(frozen=True)
class Snowflake:  # pragma: no cover
    """Snowflake class."""

    timestamp: int
    instance: int
    epoch: int = 0
    seq: int = 0

    def __post_init__(self):
        # type: () -> None
        """Validate values."""
        if self.epoch < 0:
            raise ValueError("epoch must be greater than 0!")

        if self.timestamp < 0 or self.timestamp > MAX_TS:
            raise ValueError(f"timestamp must not be negative " f"and must be less than {MAX_TS}!")

        if self.instance < 0 or self.instance > MAX_INSTANCE:
            raise ValueError(
                f"instance must not be negative and" f" must be less than {MAX_INSTANCE}!"
            )

        if self.seq < 0 or self.seq > MAX_SEQ:
            raise ValueError(f"seq must not be negative and must be less than {MAX_SEQ}!")

    @classmethod
    def parse(cls, snowflake: int, epoch: int = 0) -> "Snowflake":
        """Parse value from snowflake instance."""
        return cls(
            epoch=epoch,
            timestamp=snowflake >> 22,
            instance=snowflake >> 12 & MAX_INSTANCE,
            seq=snowflake & MAX_SEQ,
        )

    @property
    def milliseconds(self) -> int:
        """Return milliseconds."""
        return self.timestamp + self.epoch

    @property
    def seconds(self) -> float:
        """Return seconds."""
        return self.milliseconds / 1000

    @property
    def datetime(self) -> dt.datetime:
        """Return datetime from timestamp."""
        return dt.datetime.utcfromtimestamp(self.seconds)

    def datetime_tz(self, tz: Optional[tzinfo] = None) -> dt.datetime:
        """Return datetime with timezone."""
        return dt.datetime.fromtimestamp(self.seconds, tz=tz)

    @property
    def timedelta(self) -> timedelta:
        """Return timedelta."""
        return timedelta(milliseconds=self.epoch)

    @property
    def value(self) -> int:
        """Return integer value."""
        return self.timestamp << 22 | self.instance << 12 | self.seq

    def __int__(self) -> int:
        """Return value as integer."""
        return self.value


class SnowflakeGenerator:  # pragma: no cover
    """Generate snowflake integers."""

    def __init__(
        self,
        instance: int,
        *,
        seq: int = 0,
        epoch: int = 0,
        timestamp: Optional[int] = None,
    ):
        """Instance initialization."""
        current = int(time() * 1000)

        if current - epoch >= MAX_TS:
            raise OverflowError(
                "The maximum current timestamp has "
                "been reached in selected epoch,"
                "so Snowflake cannot generate more IDs!"
            )

        timestamp = timestamp or current

        if timestamp < 0 or timestamp > current:
            raise ValueError(f"timestamp must not be negative " f"and must be less than {current}!")

        if epoch < 0 or epoch > current:
            raise ValueError(
                f"epoch must not be negative and must" f" be lower than current time {current}!"
            )

        self._epo = epoch
        self._ts = timestamp - self._epo

        if instance < 0 or instance > MAX_INSTANCE:
            raise ValueError(
                f"instance must not be negative " f"and must be less than {MAX_INSTANCE}!"
            )

        if seq < 0 or seq > MAX_SEQ:
            raise ValueError(f"seq must not be negative " f"and must be less than {MAX_SEQ}!")

        self._inf = instance << 12
        self._seq = seq

    @classmethod
    def from_snowflake(cls, sf: Snowflake) -> "SnowflakeGenerator":
        """Return iter from snowflake instance."""
        return cls(sf.instance, seq=sf.seq, epoch=sf.epoch, timestamp=sf.timestamp)

    @property
    def epoch(self) -> int:
        """Return epoch."""
        return self._epo

    def __iter__(self) -> "SnowflakeGenerator":
        """Return the generator."""
        return self

    def __next__(self) -> int:
        """Return next integer value."""
        current = int(time() * 1000) - self._epo

        if current >= MAX_TS:
            raise OverflowError(
                "The maximum current timestamp "
                "has been reached in selected epoch,"
                "so Snowflake cannot generate more IDs!"
            )

        if self._ts == current:
            if self._seq == MAX_SEQ:
                raise OverflowError(
                    "The maximum current timestamp "
                    "has been reached in selected epoch,"
                    "so Snowflake cannot generate more IDs!"
                )
            self._seq += 1
        elif self._ts > current:
            raise OverflowError(
                "The maximum current timestamp "
                "has been reached in selected epoch,"
                "so Snowflake cannot generate more IDs!"
            )
        else:
            self._seq = 0

        self._ts = current

        return self._ts << 22 | self._inf | self._seq


id_generator = SnowflakeGenerator(instance=NODE_INSTANCE)


def get_next_id():
    # type: () -> int
    """Return next integer."""
    return next(id_generator)


if __name__ == "__main__":
    print(get_next_id())
