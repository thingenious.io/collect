"""Authentication."""

import secrets

from fastapi import Depends, HTTPException, Request, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials, OAuth2PasswordBearer

# we allow either Authorization: Basic or Authorization: Bearer
from itsdangerous import BadData, BadSignature, SignatureExpired, URLSafeTimedSerializer

from app.settings import settings

bearer_security = OAuth2PasswordBearer(tokenUrl="api/token", auto_error=False)
basic_security = HTTPBasic(auto_error=False)


def get_token(client_id: str) -> str:
    """Get token from form data."""
    serializer = URLSafeTimedSerializer(settings.secret_key, salt=settings.token_salt)
    token = serializer.dumps({"client_id": client_id})
    if isinstance(token, bytes):
        token = token.decode("utf-8")
    return token


async def validate_basic(credentials: HTTPBasicCredentials) -> str | None:
    """Get username from basic auth credentials."""
    if credentials.username != settings.client_id or not secrets.compare_digest(
        credentials.password, settings.client_secret
    ):
        return None
    return credentials.username


async def validate_bearer(token: str) -> str | None:
    """Validate token."""
    serializer = URLSafeTimedSerializer(settings.secret_key, salt=settings.token_salt)
    # pylint: disable=raise-missing-from
    try:
        data = serializer.loads(token, max_age=settings.token_expiration_seconds)
    except (BadSignature, BadData, SignatureExpired):
        return None
    if "client_id" not in data or not isinstance(data["client_id"], str):
        return None
    if not secrets.compare_digest(data["client_id"], settings.client_id):
        return None
    return data["client_id"]


async def security(
    request: Request,
    basic: HTTPBasicCredentials | None = Depends(basic_security),
    bearer: str | None = Depends(bearer_security),
) -> str:
    """Validate basic or bearer."""
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authenticated")
    if basic:
        client_id = await validate_basic(credentials=basic)
    elif bearer:
        client_id = await validate_bearer(token=bearer)
    else:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authenticated")
    if not client_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Not authenticated")
    return client_id
