"""App settings."""
from pathlib import Path
from typing import List

from pydantic import ValidationError, validator
from pydantic_settings import BaseSettings

STATIC_ROOT = Path(__file__).parent.parent / "static"
MEDIA_ROOT = Path(__file__).parent.parent / "media"
TEMPLATES_ROOT = Path(__file__).parent.parent / "templates"


class DatabaseSettings(BaseSettings):
    """Database settings."""

    db_use_sqlite: bool = False
    db_host: str = "localhost"
    db_port: int = 5432
    db_user: str = "postgres"
    db_password: str = ""
    db_name: str = "postgres"

    @property
    def url(self) -> str:
        """Generate DSN."""
        if self.db_use_sqlite:
            return "sqlite+aiosqlite:///./db.sqlite"
        return (
            "postgresql+asyncpg://"
            f"{self.db_user}:{self.db_password}@{self.db_host}:{self.db_port}/{self.db_name}"
        )


class Settings(DatabaseSettings):
    """App settings."""

    domain_name: str = ""
    client_id: str = ""
    client_secret: str = ""
    secret_key: str = ""
    token_salt: str = "collect"
    token_expiration_seconds: int = 3600
    http_port: int = 8000
    http_host: str = "0.0.0.0"  # nosemgrep  # nosec
    # comma-separated list of origins
    cors_origins: str | List[str] = ["*"]
    # comma-separated list of hosts
    trusted_hosts: str | List[str] = ["*"]
    # comma-separated list of regex-patterns
    cors_origins_regex: str | List[str] = []
    debug: bool = False
    static_path: Path = STATIC_ROOT
    media_path: Path = MEDIA_ROOT
    templates_path: Path = TEMPLATES_ROOT

    @property
    def database(self) -> DatabaseSettings:
        """Get database settings."""
        return DatabaseSettings(
            db_use_sqlite=self.db_use_sqlite,
            db_host=self.db_host,
            db_port=self.db_port,
            db_user=self.db_user,
            db_password=self.db_password,
            db_name=self.db_name,
        )

    # pylint: disable=too-few-public-methods
    class Config:
        """Pydantic config."""

        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False

    @validator("domain_name", pre=True)
    @classmethod
    def strip_domain_name(cls, v: str) -> str:
        """Strip whitespace from domain name."""
        domain_name = v.strip()
        if not domain_name:
            raise ValidationError(["Domain name cannot be empty"], Settings)
        return domain_name

    @validator("client_id", pre=True)
    @classmethod
    def strip_client_id(cls, v: str) -> str:
        """Strip whitespace from client id."""
        client_id = v.strip()
        if not client_id:
            raise ValidationError(["Client id cannot be empty"], Settings)
        return client_id

    @validator("client_secret", pre=True)
    @classmethod
    def strip_client_secret(cls, v: str) -> str:
        """Strip whitespace from client secret."""
        client_secret = v.strip()
        if not client_secret:
            raise ValidationError(["Client secret cannot be empty"], Settings)
        return client_secret

    @validator("secret_key", pre=True)
    @classmethod
    def strip_secret_key(cls, v: str) -> str:
        """Strip whitespace from secret key."""
        secret_key = v.strip()
        if not secret_key:
            raise ValidationError(["Secret key cannot be empty"], Settings)
        return secret_key

    @validator("cors_origins_regex", pre=True)
    @classmethod
    def split_cors_origins_regex(cls, v: str | List[str]) -> List[str]:
        """Split comma-separated list of origins."""
        if not v:
            return []
        if isinstance(v, list):
            v = ",".join(v)
        origins = [origin.strip() for origin in v.split(",")]
        return origins or []

    @validator("cors_origins", pre=True)
    @classmethod
    def split_cors_origins(cls, v: str | List[str]) -> List[str]:
        """Split comma-separated list of origins."""
        if not v:
            return ["*"]
        if isinstance(v, list):
            v = ",".join(v)
        origins = [origin.strip() for origin in v.split(",")]
        return origins or ["*"]

    @validator("trusted_hosts", pre=True)
    @classmethod
    def split_trusted_hosts(cls, v: str | List[str]) -> List[str]:
        """Split comma-separated list of hosts."""
        if not v:
            return ["*"]
        if isinstance(v, list):
            v = ",".join(v)
        hosts = [host.strip() for host in v.split(",")]
        # make sure we don't have the protocol in the string
        hosts = [host.split("://")[-1] for host in hosts]
        return hosts or ["*"]


settings = Settings()
