"""API routes."""
from fastapi import APIRouter, Depends, File, Form, HTTPException, Query, UploadFile
from fastapi.requests import Request
from fastapi.security import HTTPBasicCredentials
from sqlalchemy import select

from app.auth import get_token, security, validate_basic
from app.models import Location as DbLocation
from app.schema import (
    LocationRequest,
    LocationResponse,
    PaginatedResponse,
    TokenResponse,
    UploadResponse,
)
from app.utils import database, paginate, save_uploaded_file

router = APIRouter()


@router.post("/token")
async def login(client_id: str = Form(...), client_secret: str = Form()) -> TokenResponse:
    """Get a token."""
    credentials = HTTPBasicCredentials(username=client_id, password=client_secret)
    client = await validate_basic(credentials)
    if not client:
        raise HTTPException(status_code=400, detail="Incorrect client_id or client_secret")
    return TokenResponse(access_token=get_token(client))


@router.post("/location")
async def upload_location(body: LocationRequest, _: str = Depends(security)) -> LocationResponse:
    """Upload a location."""
    async with database.session() as session:
        async with session.begin():
            location = body.location
            db_location = DbLocation(
                client_id=body.id,
                timestamp=body.timestamp,
                latitude=location.latitude,
                longitude=location.longitude,
                accuracy=location.accuracy,
                altitude=location.altitude,
                altitudeAccuracy=location.altitudeAccuracy,
                heading=location.heading,
                speed=location.speed,
                extras=body.extras or {},
            )
            session.add(db_location)
            await session.flush()
            await session.refresh(db_location)
            return LocationResponse.from_db(db_location)


@router.get("/location")
async def get_locations(
    request: Request,
    _: str = Depends(security),
    limit: int = Query(20, ge=0, le=200),
    offset: int = Query(0, ge=0),
) -> PaginatedResponse[LocationResponse]:
    """Get all locations."""
    results = await paginate(
        request=request, query=select(DbLocation), limit=limit, offset=offset, item=LocationResponse
    )
    return results


@router.post("/image")
async def upload_image_file(
    _: str = Depends(security), file: UploadFile = File(...)
) -> UploadResponse:
    """Upload an image file."""
    return await save_uploaded_file(file=file, file_type="image")


@router.post("/audio")
async def upload_audio_file(
    _: str = Depends(security), file: UploadFile = File(...)
) -> UploadResponse:
    """Upload a audio file."""
    return await save_uploaded_file(file=file, file_type="audio")


@router.post("/video")
async def upload_video_file(
    _: str = Depends(security), file: UploadFile = File(...)
) -> UploadResponse:
    """Upload a video file."""
    return await save_uploaded_file(file=file, file_type="video")


@router.post("/motion")
async def upload_motion_file(
    _: str = Depends(security), file: UploadFile = File(...)
) -> UploadResponse:
    """Upload a motion file."""
    return await save_uploaded_file(file=file, file_type="motion")
