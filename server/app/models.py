"""SQLAlchemy models."""
# pylint: disable=too-few-public-methods
from typing import Any, Dict, Optional

from sqlalchemy import JSON, Float, Integer, String
from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from app.snowflake import get_next_id


class Base(AsyncAttrs, DeclarativeBase):
    """Base class for models."""


class Location(Base):
    """Location model."""

    __tablename__ = "locations"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, default=get_next_id)
    client_id: Mapped[str] = mapped_column(String, index=True)
    timestamp: Mapped[int] = mapped_column(Integer)
    latitude: Mapped[float] = mapped_column(Float)
    longitude: Mapped[float] = mapped_column(Float)
    accuracy: Mapped[Optional[float]] = mapped_column(Float, nullable=True)
    altitude: Mapped[Optional[float]] = mapped_column(Float, nullable=True)
    altitudeAccuracy: Mapped[Optional[float]] = mapped_column(Float, nullable=True)
    heading: Mapped[Optional[float]] = mapped_column(Float, nullable=True)
    speed: Mapped[Optional[float]] = mapped_column(Float, nullable=True)
    extras: Mapped[Dict[str, Any]] = mapped_column(JSON, nullable=False, default={})
