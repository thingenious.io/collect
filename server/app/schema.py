"""Table schemas."""

# Let's only have one single table to handle location uploads.
# the schema should follow the `Geolocation API Specification`
# <https://developer.mozilla.org/en-US/docs/Web/API/GeolocationPosition>

# pylint: disable=too-few-public-methods

import uuid
from abc import abstractmethod
from datetime import datetime, timedelta
from typing import Any, Dict, Generic, List, Optional, TypeVar
from zoneinfo import ZoneInfo

from pydantic import BaseModel, Field, validator
from pydantic_extra_types.coordinate import Latitude, Longitude

from app.models import Location as DbLocation

Item = TypeVar("Item")


class LocationInDb(DbLocation):
    """Location schema."""

    class Config:
        """Pydantic config."""

        orm_mode = True


class GeoLocationCoordinates(BaseModel):
    """Location schema.

    As specified in <https://developer.mozilla.org/en-US/docs/Web/API/GeolocationCoordinates>
    """

    latitude: Latitude = Field(..., examples=[37.4219983])
    longitude: Longitude = Field(..., examples=[-122.084])
    altitude: float | None = Field(default=None, examples=[None])
    accuracy: float | None = Field(default=None, examples=[None])
    altitudeAccuracy: float | None = Field(default=None, examples=[None])
    heading: float | None = Field(default=None, examples=[None])
    speed: float | None = Field(default=None, examples=[None])


class LocationRequest(BaseModel):
    """Location upload schema."""

    id: str = Field(..., examples=[str(uuid.uuid4())])
    timestamp: int = Field(..., examples=[int(datetime.utcnow().timestamp())])
    location: GeoLocationCoordinates
    extras: Optional[Dict[str, Any]] = Field(default=None, examples=[None])

    @validator("timestamp")
    @classmethod
    def validate_timestamp(cls, timestamp: int) -> int:
        """Validate timestamp."""
        try:
            dt = datetime.fromtimestamp(timestamp, tz=ZoneInfo("UTC"))
        except ValueError:
            # was in milliseconds?
            timestamp = timestamp // 1000
        try:
            dt = datetime.fromtimestamp(timestamp, tz=ZoneInfo("UTC"))
        except ValueError as error:
            raise ValueError(str(error)) from error
        now = datetime.now(tz=ZoneInfo("UTC"))
        if dt > now:
            raise ValueError("Timestamp cannot be in the future")
        # or if it is too old
        if dt < now - timedelta(days=1):
            raise ValueError("Timestamp is too old")
        return timestamp


class LocationOut(GeoLocationCoordinates):
    """Location schema."""

    location_id: str


class SchemaWithFromDb:
    """Schema with from_db method."""

    @classmethod
    @abstractmethod
    def from_db(cls, data: Any) -> "SchemaWithFromDb":
        """Create anew instance from a db result."""


class LocationResponse(BaseModel, SchemaWithFromDb):
    """Location response schema."""

    id: int
    timestamp: int
    location: LocationOut
    extras: Optional[Dict[str, Any]] = Field(default=None, examples=[None])

    @classmethod
    def from_db(cls, data: DbLocation) -> "LocationResponse":
        """Create a location upload from a db result."""
        return cls(
            id=data.id,
            timestamp=data.timestamp,
            location=LocationOut(
                location_id=data.client_id,
                latitude=Latitude(data.latitude),
                longitude=Longitude(data.longitude),
                accuracy=data.accuracy,
                altitude=data.altitude,
                altitudeAccuracy=data.altitudeAccuracy,
                heading=data.heading,
                speed=data.speed,
            ),
            extras=data.extras or {},
        )


class ResponseLinks(BaseModel):
    """Response links schema."""

    next: Optional[str] = Field(
        description="URL to the next page of items if available, null otherwise"
    )
    previous: Optional[str] = Field(
        description="URL to the previous page of items if available, null otherwise"
    )


class PaginatedResponse(BaseModel, Generic[Item]):
    """Paginated response schema."""

    count: int = Field(description="Number of items returned in the response")
    items: List[Item] = Field(
        description="List of items returned in the response following given criteria"
    )
    links: ResponseLinks = Field(description="Links to other pages of items")

    model_config = {
        "arbitrary_types_allowed": True,
    }


class UploadResponse(BaseModel):
    """Response when uploading a file."""

    filename: str


class TokenResponse(BaseModel):
    """Token response schema."""

    access_token: str
    expires_in: int = 3600
    token_type: str = "bearer"
