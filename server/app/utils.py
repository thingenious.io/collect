"""App utils."""
from contextlib import asynccontextmanager
from typing import Any, AsyncIterator, Tuple, Type

import aiofiles
from fastapi import FastAPI, HTTPException, Request, UploadFile
from sqlalchemy import Select, func, select

from app.db import Connection
from app.schema import PaginatedResponse, ResponseLinks, SchemaWithFromDb, UploadResponse
from app.settings import settings

database = Connection.from_db_url(settings.database.url)


@asynccontextmanager
async def lifespan(_: FastAPI) -> AsyncIterator[None]:
    """Lifespan context manager."""
    # pylint: disable=too-many-try-statements
    try:
        await database.create_metadata()
        yield
    except Exception as exc:  # pragma: no cover
        raise exc
    finally:
        await database.disconnect()


async def save_uploaded_file(file: UploadFile, file_type: str) -> UploadResponse:
    """Save uploaded audio or video."""
    filename = file.filename
    if not filename:
        raise HTTPException(status_code=400, detail=f"Invalid {file_type} file")
    allowed_extensions = {
        "image": ("png", "jpg", "jpeg"),
        "audio": ("mp4", "webm", "m4a", "ogg", "wav"),
        "video": ("mp4", "webm", "avi", "mov", "ogv"),
        "motion": ("json",),
    }
    if file_type not in allowed_extensions:
        raise HTTPException(status_code=400, detail=f"Invalid {file_type} file")
    extension = filename.split(".")[-1]
    if extension not in allowed_extensions[file_type]:
        raise HTTPException(status_code=400, detail=f"Invalid {file_type} file")
    destination_dir = settings.media_path / file_type
    destination_dir.mkdir(parents=True, exist_ok=True)
    destination = destination_dir / filename
    async with aiofiles.open(destination, "wb") as out_file:
        while content := await file.read(1024):
            await out_file.write(content)
    return UploadResponse(filename=filename)


async def paginate(
    request: Request,
    query: Select[Tuple[Any]],
    limit: int,
    offset: int,
    item: Type[SchemaWithFromDb],
) -> PaginatedResponse[Any]:
    """Paginate a query."""
    async with database.session() as session:
        db_items = await session.execute(query.limit(limit).offset(offset))
        # https://github.com/pylint-dev/pylint/issues/8138
        # pylint: disable=not-callable
        count_result = await session.execute(select(func.count()).select_from(query.subquery()))
        count = count_result.scalar() or 0
        next_offset = offset + limit if count > offset + limit else None
        previous_offset = offset - limit if offset - limit >= 0 else None
        next_link = (
            str(
                request.url.include_query_params(
                    limit=limit, offset=next_offset
                ).remove_query_params("cursor")
            )
            if next_offset is not None
            else None
        )
        previous_link = (
            str(
                request.url.include_query_params(
                    limit=limit, offset=previous_offset
                ).remove_query_params("cursor")
            )
            if previous_offset is not None
            else None
        )
        return PaginatedResponse[SchemaWithFromDb](
            count=count,
            items=[item.from_db(row) for row in db_items.scalars().all()],
            links=ResponseLinks(
                next=next_link,
                previous=previous_link,
            ),
        )
