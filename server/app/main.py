"""Application entrypoint."""
import os
from pathlib import Path

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from fastapi.requests import Request
from fastapi.responses import RedirectResponse, Response
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from app.api import router as api_router
from app.settings import settings
from app.utils import lifespan

templates = Jinja2Templates(directory=settings.templates_path)

app = FastAPI(
    debug=settings.debug,
    openapi_url="/openapi.json",
    title="Collect API",
    docs_url=None,
    redoc_url=None,
    lifespan=lifespan,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.cors_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_origin_regex=settings.cors_origins_regex or None,
)
app.add_middleware(
    TrustedHostMiddleware,
    allowed_hosts=settings.trusted_hosts,
)

# Mount the API router.
app.include_router(api_router, prefix="/api")
# Mount the static files router.
app.mount("/static", StaticFiles(directory=settings.static_path), name="static")


@app.get("/favicon.ico", include_in_schema=False)
async def favicon() -> RedirectResponse:
    """Redirect favicon."""
    return RedirectResponse(url="/static/icons/favicon.ico")


@app.get("/docs", include_in_schema=False)
async def get_docs(request: Request) -> Response:
    """Get the API docs."""
    return templates.TemplateResponse(
        "docs.html", {"request": request, "schema_url": "/openapi.json"}
    )


@app.get("/", include_in_schema=False)
def read_root() -> Response:
    """Redirect to API docs."""
    return RedirectResponse(url="/docs")


def main() -> None:
    """Run the application."""
    # for reload we need to use import string for the app
    # and not app directly
    # let's chdir to use "{dotted}.{module}:app" syntax
    here = Path(__file__).parent.absolute()
    parent = here.parent.absolute()
    this_module = Path(__file__).name.replace(".py", "")
    os.chdir(parent)
    module_to_import = f"{here.name}.{this_module}:app"
    uvicorn.run(
        module_to_import,
        host=settings.http_host,
        port=settings.http_port,
        reload=settings.debug,
        reload_dirs=[str(here)] if settings.debug else None,
        log_level="debug" if settings.debug else "info",
    )


if __name__ == "__main__":
    main()
