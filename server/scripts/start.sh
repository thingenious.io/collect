#!/usr/bin/env sh
# shellcheck disable=SC1091,SC1090
set -e

HERE="$(dirname "$(readlink -f "$0")")"
ROOT_DIR="$(dirname "${HERE}")"
cd "${ROOT_DIR}" || exit 1
if [ -f "${HERE}/pre_start.py" ]; then
    echo "Running pre-start actions..."
    python3 "${HERE}/pre_start.py" || true
fi

echo "Starting server..."
python3 -m app.main
