"""Actions to perform before starting the app."""

import asyncio
import os

from sqlalchemy import text
from sqlalchemy.ext.asyncio import create_async_engine
from tenacity import retry, stop_after_attempt, wait_fixed

# Postgresql
DB_HOST = os.getenv("DB_HOST", "localhost")
DB_PORT = os.getenv("DB_PORT", "5432")
DB_USER = os.getenv("DB_USER", "postgres")
DB_PASSWORD = os.getenv("DB_PASSWORD", "")
DB_NAME = os.getenv("DB_NAME", "postgres")
_DB_USE_SQLITE = os.getenv("DB_USE_SQLITE", "false")
if not _DB_USE_SQLITE:
    _DB_USE_SQLITE = "false"
DB_USE_SQLITE = _DB_USE_SQLITE.lower()[0] in ("t", "y", "1")

MAX_RETRIES = 60 * 5  # 5 minutes
WAIT_SECONDS = 5


@retry(
    stop=stop_after_attempt(MAX_RETRIES),
    wait=wait_fixed(WAIT_SECONDS),
)  # type: ignore
async def pg_init() -> None:
    """Wait for postgresql."""
    db_url = f"postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    # pylint: disable=too-many-try-statements
    try:
        engine = create_async_engine(db_url)
        async with engine.begin() as conn:
            query = "SELECT 1"
            await conn.execute(text(query))
    except Exception as e:
        print(e)
        raise e
    print("Postgresql connection OK")


async def main() -> None:
    """Check database connection if needed."""
    if DB_USE_SQLITE:
        print("No need to check connection")
    else:
        print("Checking Postgresql connection")
        await pg_init()
        print("Postgresql connection OK")


if __name__ == "__main__":
    asyncio.run(main())
